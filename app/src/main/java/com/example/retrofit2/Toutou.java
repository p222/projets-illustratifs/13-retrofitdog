
package com.example.retrofit2;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Toutou implements Serializable
{

    @SerializedName("message")
    @Expose
    private String message;


    @SerializedName("status")
    @Expose
    private String status;


    private final static long serialVersionUID = 4514351702960150919L;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
