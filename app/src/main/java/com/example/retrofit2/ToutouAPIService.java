package com.example.retrofit2;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ToutouAPIService {
    @GET("breeds/image/random")
    Call<Toutou> appelAPI();
}
