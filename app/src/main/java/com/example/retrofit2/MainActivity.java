package com.example.retrofit2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    TextView text ;
    final static String BASE_URL_DOG ="https://dog.ceo/api/";
    ImageView image;

    // Retrofit
    Retrofit retrofit;
    ToutouAPIService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (TextView) findViewById(R.id.valeur);
        Button  bouton = (Button) findViewById(R.id.button);
        image = (ImageView)findViewById(R.id.imageView);

        this.retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_DOG)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiService = retrofit.create(ToutouAPIService.class);

        // Traitement du clic sur le bouton
        bouton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<Toutou> appel =  apiService.appelAPI();

                appel.enqueue(new Callback<Toutou>() {
                    @Override
                    public void onResponse(Call<Toutou> call, Response<Toutou> response) {
                        if (response.isSuccessful()){
                            Toutou medor = response.body();
                            Toast.makeText(MainActivity.this, medor.getMessage(), Toast.LENGTH_SHORT).show();
                            text.setText(medor.getMessage());

                            new DownloadImageTask(image).execute(medor.getMessage());

                        }
                        else{
                            Toast.makeText(MainActivity.this, "Erreur API", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Toutou> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Erreur de connexion", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }


    // Permet d'extraire une image à partir d'une URL via une tâche asynchrone (en tâche de fond)
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bmp = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bmp = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bmp;
        }
        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}

